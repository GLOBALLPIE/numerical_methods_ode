import numpy as np
import math


def twostage_step(f, x0, y0, h):
    c2 = 0.25
    a21 = c2
    b2 = 1 / (2 * c2)
    b1 = 1 - 1 / (2 * c2)
    k1 = f(x0, y0)
    k2 = f(x0 + c2 * h, y0 + h * a21 * k1)
    return y0 + h * (b1 * k1 + b2 * k2)


def opponent_step(f, x0, y0, h):
    k1 = f(x0, y0)
    k2 = f(x0 + 0.5 * h, y0 + 0.5 * h * k1)
    k3 = f(x0 + 0.5 * h, y0 + 0.5 * h * k2)
    k4 = f(x0 + h, y0 + h * k3)
    return y0 + h * ((1 / 6) * k1 + (1 / 3) * k2 + (1 / 3) * k3 + (1 / 6) * k4)  # классический метод четвертого порядка

    # return y0 + h * f(x0 + 0.5 * h, y0 + 0.5 * h * f(x0, y0))  # метод Эйлера
    # return y0 + 0.5 * h * (f(x0, y0) + f(x0 + h, y0 + h * f(x0, y0)))  # метод Хойна

    # k1 = f(x0, y0)
    # k2 = f(x0 + (1 / 3) * h, y0 + (1 / 3) * h * k1)
    # k3 = f(x0 + (2 / 3) * h, y0 + (2 / 3) * h * k2)
    # return y0 + 0.5 * h * ((1 / 4) * k1 + (3 / 4) * k3)  # метод Хойна третьего порядка

    # k1 = f(x0, y0)
    # k2 = f(x0 + 0.5 * h, y0 + 0.5 * h * k1)
    # k3 = f(x0 + h, y0 - h * k1 + 2 * h * k2)
    # return y0 + h * (1 / 6 * k1 + 4 / 6 * k2 + 1 / 6 * k3) # Метод Симпсона

    k1 = f(x0, y0)
    k2 = f(x0 + 1 / 3 * h, y0 + 1 / 3 * h * k1)
    k3 = f(x0 + 2 / 3 * h, y0 - 1 / 3 * h * k1 + h * k2)
    k4 = f(x0 + h, y0 + h * k1 - h * k2 + h * k3)
    return y0 + h * (1 / 8 * k1 + 3 / 8 * k2 + 3 / 8 * k3 + 1 / 8 * k4)  # 3/8


def euler_step(f, x0, y0, h):
    return y0 + h * f(x0 + 0.5 * h, y0 + 0.5 * h * f(x0, y0))  # метод Эйлера


def solve(solver, f, x0, y0, x_end, N):
    success = True
    i = 1
    if solver == 'twostage':
        make_step = twostage_step
    elif solver == 'opponent':
        make_step = opponent_step
    else:
        raise KeyError('Неправильное название метода')
    x, h = np.linspace(x0, x_end, N + 1, retstep=True)
    y = np.array([y0])
    for xi in x[:-1]:
        step = make_step(f, xi, y[-1], h)
        # if np.sum(np.isnan(step)) > 0:
        #     success = False
        #     break
        # else:
        #     y = np.vstack((y, step))
        #     i += 1

        y = np.vstack((y, step))
        i += 1
    if np.sum(np.isnan(step)) > 0:
        success = False
    return x[:i], np.array(y), success


def get_r2n(yn, y2n, p):
    return (y2n - yn) / (2 ** p - 1)


def get_opt_h(hn, tol, yn, y2n, p):
    rn = (y2n - yn) / (1 - 2 ** (-p))
    r2n = (y2n - yn) / (2 ** p - 1)
    return hn * np.power(tol / np.linalg.norm(rn), 1 / p)
    # return 0.5 * hn * np.power(tol / np.linalg.norm(r2n), 1 / p)


def solve_wtih_h(solver, h, f, x0, y0, x_end):
    if solver == 'twostage':
        make_step = twostage_step
    elif solver == 'opponent':
        make_step = opponent_step
    else:
        raise KeyError('Неправильное название метода')
    xi = x0
    x = np.array([x0])
    y = np.array([y0])
    while xi < x_end:
        step = make_step(f, xi, y[-1], h)
        y = np.vstack((y, step))
        x = np.hstack((x, xi + h))
        xi += h
    return x, y


def solve_with_h_opt(solver, p, tol, h, yn, y2n, f, x0, y0, x_end):
    if solver == 'twostage':
        make_step = twostage_step
    elif solver == 'opponent':
        make_step = opponent_step
    else:
        raise KeyError('Неправильное название метода')

    h_opt = get_opt_h(h, tol, yn, y2n, p)
    xi = x0
    x = np.array([x0])
    y = np.array([y0])
    while xi < x_end:
        step = make_step(f, xi, y[-1], h_opt)
        y = np.vstack((y, step))
        x = np.hstack((x, xi + h_opt))
        xi += h_opt
    return x, y, h_opt


def get_first_step(p, tol, f, x0, y0, x_end):
    delta1 = np.power(1 / max(np.abs(x0), np.abs(x_end)), p + 1) + np.linalg.norm(f(x0, y0)) ** (p + 1)
    h1 = np.power(tol / delta1, 1 / (p + 1))
    u1 = euler_step(f, x0, y0, h1)

    delta2 = np.power(1 / max(np.abs(x0 + h1), np.abs(x_end)), p + 1) + np.linalg.norm(f(x0 + h1, y0 + u1)) ** (p + 1)
    h2 = np.power(tol / delta2, 1 / (p + 1))
    return min(h1, h2)


def optimal_method(solver, p, rtol, atol, f, x0, y0, x_end):
    if solver == 'twostage':
        make_step = twostage_step
    elif solver == 'opponent':
        make_step = opponent_step
    else:
        raise KeyError('Неправильное название метода')

    xi = x0
    yi = np.array(y0)
    tol = rtol * np.linalg.norm(yi) + atol
    h = get_first_step(p, tol, f, x0, y0, x_end)
    # h = 1 / (2 ** 8)
    x = np.array(x0)
    y = np.array(y0)
    y_old = make_step(f, xi, yi, h)
    y_middle = make_step(f, xi, yi, h / 2)
    y_new = make_step(f, xi + h / 2, y_middle, h / 2)
    err_old = (y_new - y_old) / (1 - 2 ** (-p))
    err_new = (y_new - y_old) / ((2 ** p) - 1)
    h_current = h
    h_next = h
    h_max = h_current

    while xi < x_end:
        if np.linalg.norm(err_old) > tol * (2 ** p):
            h_current /= 2
            y_old = y_middle
            y_middle = make_step(f, xi, yi, h_current / 2)
            y_new = make_step(f, xi + h_current / 2, y_middle, h_current / 2)
            err_old = (y_new - y_old) / (1 - 2 ** (-p))
            err_new = (y_new - y_old) / ((2 ** p) - 1)
            # h_max = max(h_max, h_current)
            continue
        else:
            if tol < np.linalg.norm(err_old) <= tol * (2 ** p):
                h_next = h_current / 2
                x = np.hstack((x, xi + h_current))
                y = np.vstack((y, y_new + err_new))
                yi = y_new + err_new
                h_max = max(h_max, h_current)
            if tol * (2 ** (-p - 1)) <= np.linalg.norm(err_old) <= tol:
                h_next = h_current
                x = np.hstack((x, xi + h_current))
                y = np.vstack((y, y_old + err_old))
                yi = y_old + err_old
                h_max = max(h_max, h_current)
            if np.linalg.norm(err_old) < tol * (2 ** (-p - 1)):
                h_next = min(2 * h_current, h_max)
                x = np.hstack((x, xi + h_current))
                y = np.vstack((y, y_old + err_old))
                yi = y_old + err_old
                h_max = max(h_max, h_current)

        xi = xi + h_current
        h_current = h_next
        y_old = make_step(f, xi, yi, h_current)
        y_middle = make_step(f, xi, yi, h_current / 2)
        y_new = make_step(f, xi + h_current / 2, y_middle, h_current / 2)
        err_old = (y_new - y_old) / (1 - 2 ** (-p))
        err_new = (y_new - y_old) / ((2 ** p) - 1)
        tol = rtol * np.linalg.norm(yi) + atol
    return x, y

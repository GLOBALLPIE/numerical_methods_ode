import numpy as np
from methods import rungekutta
import matplotlib.pyplot as plt
import math

A = 1
B = 1.5
C = -2


def correct_pow(a, B):  # Реализовано для B=1.5
    return np.power(np.abs(a), 1 / B)


def f(x, y):
    return np.array(
        [2 * x * correct_pow(y[1], B) * y[3],
         2 * B * x * np.exp((B / C) * (y[2] - A)) * y[3],
         2 * C * x * y[3],
         -2 * x * np.log(y[0])])


def exact_func(x):
    return np.array(
        [np.exp(np.sin(np.power(x, 2))),
         np.exp(B * np.sin(np.power(x, 2))),
         C * np.sin(np.power(x, 2)) + A,
         np.cos(np.power(x, 2))])


params = {'f': f, 'x0': 0, 'y0': np.array([1, 1, A, 1]), 'x_end': 5}


def plot_norms(solver):
    k_range = np.arange(4, 12)
    norms = []
    for k in k_range:
        # x, y, success = rungekutta.solve(solver, params['f'], params['x0'], params['y0'], params['x_end'],
        #                                  (params['x_end'] - params['x0']) * (2 ** k))
        x, y = rungekutta.solve_wtih_h(solver, 1 / (2 ** k), **params)
        err = np.linalg.norm(exact_func(x[-1]) - y[-1])
        norms.append(-np.log2(err))
        print(-np.log2(1 / 2 ** k), -np.log2(err))

    k_range = -np.log2(1 / 2 ** k_range)
    plt.grid()
    plt.plot(k_range, norms)
    plt.legend()
    plt.title(solver)
    plt.xlabel('x')
    plt.ylabel('y(x)')
    plt.show()


plot_norms('twostage')
plot_norms('opponent')

import numpy as np
from methods import rungekutta
import matplotlib.pyplot as plt
import math

A = 1
B = 1.5
C = -2


def correct_pow(a, B):  # Реализовано для B=1.5
    return np.power(np.abs(a), 1 / B)


def f(x, y):
    return np.array(
        [2 * x * correct_pow(y[1], B) * y[3],
         2 * B * x * np.exp((B / C) * (y[2] - A)) * y[3],
         2 * C * x * y[3],
         -2 * x * np.log(y[0])])


def exact_func(x):
    return np.array(
        [np.exp(np.sin(np.power(x, 2))),
         np.exp(B * np.sin(np.power(x, 2))),
         C * np.sin(np.power(x, 2)) + A,
         np.cos(np.power(x, 2))])


params = {'f': f, 'x0': 0, 'y0': np.array([1, 1, A, 1]), 'x_end': 5}

k = 7
solver = 'twostage'
tol = 1e-5
p = 2
h_old = 1 / (2 ** k)
x0, y0 = rungekutta.solve_wtih_h(solver, h_old, **params)

x1, y1 = rungekutta.solve_wtih_h(solver, h_old / 2, **params)

x_opt, y_opt, h_opt = rungekutta.solve_with_h_opt(solver, p, tol, h_old, y0[-1], y1[-1], params['f'],
                                                  params['x0'], params['y0'], params['x_end'])
y_exact = exact_func(x_opt).T
print('h_old: ', h_old)
err = y_exact - y_opt
print('h_opt: ', h_opt)
err = np.power(np.sum(np.power(err, 2), axis=1), 0.5)
err = -np.log10(err)

plt.grid()
plt.plot(x_opt, y_opt)
plt.legend()
plt.title(solver)
plt.xlabel('x')
plt.ylabel('y(x)')
plt.show()

plt.grid()
plt.plot(x_opt, err)
plt.legend()
plt.title(solver + ' error')
plt.xlabel('x')
plt.ylabel('err(x)')
plt.show()
